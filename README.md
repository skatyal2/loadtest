**To test HPA after deplying manually:**
- clone the repo:
- `git clone https://gitlab.com/redhat-kb/loadtest.git && cd loadtest/loadtest`


- launch the loadtest application:
`oc create --save-config -f ./loadtest.yaml`


- Set horizontal pod autoscaler (optional):
`oc autoscale deployment/loadtest --min 2 --max 10 --cpu-percent 50`

- Open a new terminal tab and monitor:
`watch -n 1 'oc get hpa/loadtest; echo ""; oc get pods -l app=loadtest'`


- apply load on CPU:
```curl -X GET http://`oc get route loadtest -o jsonpath='{.status.ingress[*].host}'`/api/loadtest/v1/cpu/1```


- apply load on memory:

```curl -X GET http://`oc get route loadtest -o jsonpath='{.status.ingress[*].host}'`/api/loadtest/v1/mem/200/60```


**To intigrate with Argo-CD for automatic deployment:**
- here is the snippet to apply in Argo-CD.
```
apiVersion: argoproj.io/v1alpha1
kind: Application
metadata:
  name: gitops-test-app
spec:
  project: default
  source:
    repoURL: 'https://gitlab.com/bharatbabbar28/loadtest.git'
    path: loadtest
    targetRevision: HEAD
  destination:
    server: 'https://kubernetes.default.svc'
    namespace: argocd-test
  syncPolicy:
    automated:
      prune: true
      selfHeal: true
    syncOptions:
      - CreateNamespace=true
    managedNamespaceMetadata:
      labels:
        argocd.argoproj.io/managed-by: openshift-gitops
  ignoreDifferences:
    - group: apps
      kind: Deployment
      jsonPointers:
        - /spec/replicas
    - group: route.openshift.io
      kind: Route
      jsonPointers:
        - /spec/host
        - /spec/to/kind
        - /spec/subdomain
```
